﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulse : MonoBehaviour {

    public float pulseTime;

	void Start () {
        iTween.ScaleTo(gameObject, iTween.Hash("Target", gameObject, "Scale", transform.localScale * 1.2f, "Time", pulseTime, "EaseType", "easeinoutsine", "Looptype", "PingPong"));
	}
	
	
}
