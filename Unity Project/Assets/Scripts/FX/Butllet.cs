﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Butllet : MonoBehaviour {

    private Transform targetTransform;

	public void SetProjectile(Transform tempTransform)
    {
        targetTransform = tempTransform;
        StartCoroutine(Movement());
    }

    private IEnumerator Movement()
    {
        float timeElapsed = 0;
        Vector3 startingPosition = transform.position;

        while(timeElapsed < 1)
        {
            timeElapsed += Time.deltaTime * 4;

            transform.position = Vector3.Lerp(startingPosition, targetTransform.position, timeElapsed);

            yield return 0;
        }

        yield return new WaitForSeconds(1);

        Destroy(gameObject);
    }
}
