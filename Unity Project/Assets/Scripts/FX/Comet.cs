﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comet : MonoBehaviour {

    public Transform startingPosition;

    public Transform endPosition;

    public TrailRenderer trailRenderer;

	void Start ()
    {
        StartCoroutine(CometMovement());
	}

    private IEnumerator CometMovement()
    {
        float timeElapsed = 0;

        trailRenderer.Clear();
        while (timeElapsed < 1)
        {
            timeElapsed += Time.deltaTime * 0.01f;

            transform.position = Vector2.Lerp(startingPosition.position, endPosition.position, timeElapsed);

            yield return 0;
        }

        yield return new WaitForSeconds(5);

        StartCoroutine(CometMovement());
    }
}
