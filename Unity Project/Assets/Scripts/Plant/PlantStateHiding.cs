﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class PlantStateHiding : StateBehaviour
{

	private GameObjectVar petal1;

	private GameObjectVar petal2;

	GameObjectVar player;

    void Awake()
    {
		petal1 = blackboard.GetGameObjectVar ("PetalRight");
		petal2 = blackboard.GetGameObjectVar ("PetalLeft");

		player = blackboard.GetGameObjectVar ("Player");
    }

    void OnEnable()
    {
		StartCoroutine (Hidding());
    }

    void OnDisable()
    {
		StopAllCoroutines ();
    }

    void Update()
    {

    }

	private IEnumerator Hidding()
	{
		float timeElapsed = 0;
		Quaternion petal1StartRotation = petal1.transform.rotation;
		Quaternion petal2StartRotation = petal2.transform.rotation;
		while (true) {

			if (Vector2.Distance(transform.position, player.transform.position) > 2)
			{
				timeElapsed = 0;
				petal1StartRotation = petal1.transform.rotation;
				petal2StartRotation = petal2.transform.rotation;;
				while (timeElapsed < 1) {
					timeElapsed += Time.deltaTime;
					petal1.transform.rotation = Quaternion.Lerp(petal1StartRotation, Quaternion.Euler(0, 0, 0), timeElapsed);
					petal2.transform.rotation = Quaternion.Lerp(petal2StartRotation, Quaternion.Euler(0, 0, 0), timeElapsed);
					yield return 0;
				}

				SendEvent("Idling");
				break;
			}

			while (timeElapsed < 1) {
				timeElapsed += Time.deltaTime;
				petal1.transform.rotation = Quaternion.Lerp(petal1StartRotation, Quaternion.Euler(0, 0, -20), timeElapsed);
				petal2.transform.rotation = Quaternion.Lerp(petal2StartRotation, Quaternion.Euler(0, 0, 20), timeElapsed);
				yield return 0;
			}

			yield return 0;
		}
	}
}
