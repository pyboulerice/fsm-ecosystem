﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class PlantStateGrowing : StateBehaviour
{

    void Start()
    {

    }

    void OnEnable()
    {
		StartCoroutine (Grow());
    }

    void OnDisable()
    {

    }

    void Update()
    {

    }

	private IEnumerator Grow()
	{
		float timeElapsed = 0;
		while (timeElapsed <= 1) {
			timeElapsed += Time.deltaTime * 2;

			transform.localScale = Vector2.Lerp (Vector2.zero, Vector2.one, timeElapsed);
			yield return 0;

		}

		SendEvent ("Idling");
	}
}
