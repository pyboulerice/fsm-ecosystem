﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class PlantStateDying : StateBehaviour
{

    void Start()
    {

    }

    void OnEnable()
    {
		StartCoroutine (Shrink ());
    }

    void OnDisable()
    {

    }

    void Update()
    {

    }

	private IEnumerator Shrink()
	{
		float timeElapsed = 0;
		while (timeElapsed <= 1) {
			timeElapsed += Time.deltaTime * 2;

			transform.localScale = Vector2.Lerp (Vector2.one, Vector2.zero, timeElapsed);
			yield return 0;

		}

		yield return new WaitForSeconds (2);

		SendEvent ("Growing");
	}
}
