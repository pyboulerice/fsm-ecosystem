﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class PlantStateIdling : StateBehaviour
{

	private GameObjectVar petal1;

	private GameObjectVar petal2;

	private GameObjectVar player;

	private BoolVar edible;

	private float timeElapsed;

    void Awake()
    {
		petal1 = blackboard.GetGameObjectVar ("PetalRight");
		petal2 = blackboard.GetGameObjectVar ("PetalLeft");

		player = blackboard.GetGameObjectVar ("Player");

		edible = blackboard.GetBoolVar ("Edible");

		timeElapsed = Random.Range (0, 10);

    }

    void OnEnable()
    {
		StartCoroutine (Bobbing());
    }

    void OnDisable()
    {
		StopAllCoroutines ();
    }

    void Update()
    {

    }

	private IEnumerator Bobbing()
	{
		edible.Value = true;

		while (true) {

			timeElapsed += Time.deltaTime;

			petal1.transform.rotation = Quaternion.Euler (0, 0, (Mathf.Sin (timeElapsed)) * 10);
			petal2.transform.rotation = Quaternion.Euler (0, 0, (Mathf.Sin (timeElapsed) * -1) * 10);

			if (Vector2.Distance(transform.position, player.transform.position) < 2)
			{
				edible.Value = false;
				timeElapsed = 0;
				SendEvent("Hiding");
				break;
			}

			yield return 0;
		}
	}
}
