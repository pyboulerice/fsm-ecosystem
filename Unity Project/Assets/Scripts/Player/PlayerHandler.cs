﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandler : MonoBehaviour {


	[SerializeField]
	private float speed;

	void Awake () 
	{
		StartCoroutine (PlayerMovement());
	}

	private IEnumerator PlayerMovement()
	{
		while (true) {

            if (transform.position.x > EcosystemManager.Instance.leftWall.position.x)
            {
                if (Input.GetKey(KeyCode.A))
                {
                    MoveItteration(-1);
                    transform.localScale = new Vector3(-1, 1, 1);
                }
            }

            if (transform.position.x < EcosystemManager.Instance.rightWall.position.x)
            {
                if (Input.GetKey(KeyCode.D))
                {
                    MoveItteration(1);
                    transform.localScale = new Vector3(1, 1, 1);
                }
            }

			yield return 0;
		}
	}

	private void MoveItteration(float direction)
	{
		transform.Translate (direction * speed * Time.deltaTime, 0, 0);
	}
}
