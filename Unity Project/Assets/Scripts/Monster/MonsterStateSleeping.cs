﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class MonsterStateSleeping : StateBehaviour {

    [SerializeField]
    private ParticleSystem sleepingParticleSystem;

    void Start()
    {

    }

    void OnEnable()
    {
        StartCoroutine(Sleep());
    }

    void OnDisable()
    {

    }

    void Update()
    {

    }

    private IEnumerator Sleep()
    {
        sleepingParticleSystem.Play();
        yield return new WaitForSeconds(2);
        sleepingParticleSystem.Stop();
        yield return new WaitForSeconds(3);
        SendEvent("Lerking");
    }
}
