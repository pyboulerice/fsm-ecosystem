﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class MonsterStateAttacking : StateBehaviour {

    [SerializeField]
    private Transform claw;

    [SerializeField]
    private Transform showAnchor;

    [SerializeField]
    private Transform hideAnchor;

    void Awake()
    {

    }

    void OnEnable()
    {
        StartCoroutine(Attack());
    }

    void OnDisable()
    {

    }

    void Update()
    {

    }

    private IEnumerator Attack()
    {
        float timeElapsed = 0;

        while (timeElapsed < 1)
        {
            timeElapsed += Time.deltaTime * 2;

            claw.position = Vector2.Lerp(hideAnchor.position, showAnchor.position, EcosystemManager.Instance.fallDownCurve.Evaluate(timeElapsed));

            yield return 0;
        }

        timeElapsed = 0;

        while (timeElapsed < 1)
        {
            timeElapsed += Time.deltaTime;

            claw.position = Vector2.Lerp(showAnchor.position, hideAnchor.position, EcosystemManager.Instance.fallDownCurve.Evaluate(timeElapsed));

            yield return 0;
        }

        SendEvent("Lerking");
    }
}
