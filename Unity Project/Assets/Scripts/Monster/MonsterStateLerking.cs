﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class MonsterStateLerking : StateBehaviour {

    FloatVar timeElapsed;

    void Start()
    {
    }

    void OnEnable()
    {
        timeElapsed = blackboard.GetFloatVar("TimeElapsed");
        StartCoroutine(Lerking());
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    void Update()
    {

    }

    private IEnumerator Lerking()
    {
        while (timeElapsed.Value < 1)
        {
            timeElapsed.Value += Time.deltaTime * 0.25f;

            if (EcosystemManager.Instance.critters.Count > 3)
            {
                for (int i = 0; i < EcosystemManager.Instance.critters.Count; i++)
                {
                    if (Vector2.Distance(EcosystemManager.Instance.critters[i].position, transform.position) < 3)
                    {
                        Transform tempTransform = EcosystemManager.Instance.critters[i];
                        EcosystemManager.Instance.RemoveCritter(tempTransform);
                        tempTransform.GetComponent<Blackboard>().SendEvent("Died");
                        SendEvent("Attacking");
                        break;
                    }
                }
            }
            yield return 0;
        }

        StartCoroutine(Show());
    }

    private IEnumerator Show()
    {
        timeElapsed.Value = 0;

        float tempTimeElapsed = 0;

        Vector3 startingPosition = transform.position;

        while (tempTimeElapsed < 1)
        {
            tempTimeElapsed += Time.deltaTime;

            transform.position = Vector3.Lerp(new Vector3(startingPosition.x, startingPosition.y, 0), new Vector3(startingPosition.x, EcosystemManager.Instance.monsterPositionAnchors[0].position.y, 0), tempTimeElapsed);

            yield return 0;
        }

        SendEvent("Relocating");
    }
}
