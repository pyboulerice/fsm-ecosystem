﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class MonsterStateMoving : StateBehaviour {

    FloatVar monsterSpeed;

    void Awake()
    {
        
    }

    void OnEnable()
    {
        monsterSpeed = blackboard.GetFloatVar("MonsterSpeed");
        StartCoroutine(Moving());
    }

    void OnDisable()
    {

    }

    void Update()
    {

    }

    private IEnumerator Moving()
    {
        float timeElapsed = 0;

        int targetTransform = Random.Range(0, EcosystemManager.Instance.monsterPositionAnchors.Length);

        Vector3 startingPosition = transform.position;

        if (transform.position.x > EcosystemManager.Instance.monsterPositionAnchors[targetTransform].position.x)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }

        while(true)
        {
            timeElapsed += Time.deltaTime * monsterSpeed;

            transform.position = Vector3.Lerp(startingPosition, EcosystemManager.Instance.monsterPositionAnchors[targetTransform].position, EcosystemManager.Instance.monsterMovementCurve.Evaluate(timeElapsed));

            if (timeElapsed >= 1)
            {
                break;
            }

            yield return 0;
        }

        StartCoroutine(Hide());
    }

    private IEnumerator Hide()
    {
        float timeElapsed = 0;

        Vector3 startingPosition = transform.position;

        while (timeElapsed < 1)
        {
            timeElapsed += Time.deltaTime;

            transform.position = Vector3.Lerp(new Vector3(startingPosition.x, startingPosition.y, 0), new Vector3(startingPosition.x, EcosystemManager.Instance.hideAnchor.position.y, 0), timeElapsed);

            yield return 0;
        }

        SendEvent("Sleeping");
    }
}
