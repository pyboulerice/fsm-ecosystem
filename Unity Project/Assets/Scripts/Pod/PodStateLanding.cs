﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class PodStateLanding : StateBehaviour
{

    [SerializeField]
    private AnimationCurve landingCurve;

    [SerializeField]
    private ParticleSystem engineParticleSystem;

    [SerializeField]
    private ParticleSystem smokeParticleSystem;

    void Start()
    {
    }

    void OnEnable()
    { 
        StartCoroutine(Landing());
    }

    void OnDisable()
    {

    }

    void Update()
    {

    }

    private IEnumerator Landing()
    {
        yield return new WaitForSeconds(5f);
        engineParticleSystem.Play();
        smokeParticleSystem.Play();

        float timeElapsed = 0;

        Vector2 startingPosition = transform.position;

        while (timeElapsed < 1)
        {
            timeElapsed += Time.deltaTime * 0.25f;

            transform.position = Vector2.Lerp(startingPosition, new Vector2(startingPosition.x, EcosystemManager.Instance.groundAnchor.position.y), landingCurve.Evaluate(timeElapsed));

            yield return 0;
        }

        engineParticleSystem.Stop();
        smokeParticleSystem.Stop();

        SendEvent("Opening");
    }

}