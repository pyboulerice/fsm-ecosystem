﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class PodStateClosing : StateBehaviour
{

    [SerializeField]
    private Transform openAnchor;

    [SerializeField]
    private Transform closeAnchor;

    [SerializeField]
    private Transform doorAnchor;

    [SerializeField]
    private AnimationCurve doorCloseAnimationCurve;

    void Start()
    {

    }

    void OnEnable()
    {
        StartCoroutine(Close());
    }

    void OnDisable()
    {

    }

    void Update()
    {

    }

    private IEnumerator Close()
    {
        float timeElapsed = 0;

        while (timeElapsed < 1)
        {
            timeElapsed += Time.deltaTime;

            doorAnchor.position = Vector2.Lerp(openAnchor.position, closeAnchor.position, doorCloseAnimationCurve.Evaluate(timeElapsed));

            yield return 0;
        }

        SendEvent("Launching");
    }
}
