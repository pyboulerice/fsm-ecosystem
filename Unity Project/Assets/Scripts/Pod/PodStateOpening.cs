﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class PodStateOpening : StateBehaviour
{

    [SerializeField]
    private Transform openAnchor;

    [SerializeField]
    private Transform closeAnchor;
    
    [SerializeField]
    private Transform doorAnchor;

    [SerializeField]
    private AnimationCurve doorOpenAnimationCurve;

    void Awake()
    {

    }

    void OnEnable()
    {
        StartCoroutine(Open());
    }

    void OnDisable()
    {

    }

    void Update()
    {

    }

    private IEnumerator Open()
    {
        yield return new WaitForSeconds(1);

        float timeElapsed = 0;

        while (timeElapsed < 1)
        {
            timeElapsed += Time.deltaTime;

            doorAnchor.position = Vector2.Lerp(closeAnchor.position, openAnchor.position, doorOpenAnimationCurve.Evaluate(timeElapsed));

            yield return 0;
        }

        SendEvent("Attacking");
    }
}
