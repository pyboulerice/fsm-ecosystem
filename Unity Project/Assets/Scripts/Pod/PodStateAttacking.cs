﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class PodStateAttacking : StateBehaviour
{

    [SerializeField]
    private Transform showAnchor;

    [SerializeField]
    private Transform hideAnchor;

    [SerializeField]
    private Transform weaponAnchor;

    [SerializeField]
    private Transform weaponTop;

    [SerializeField]
    private AnimationCurve weaponShowAnimationCurve;

    [SerializeField]
    private GameObject projectilePrefab;

    void Start()
    {

    }

    void OnEnable()
    {
        StartCoroutine(Show());
    }

    void OnDisable()
    {

    }

    void Update()
    {

    }

    private IEnumerator Show()
    {
        float timeElapsed = 0;

        while (timeElapsed < 1)
        {
            timeElapsed += Time.deltaTime;

            weaponAnchor.position = Vector2.Lerp(hideAnchor.position, showAnchor.position, weaponShowAnimationCurve.Evaluate(timeElapsed));

            yield return 0;
        }

        StartCoroutine(Lerking());
    }

    private IEnumerator Lerking()
    {
        float timeElapsed = 0;
        while (timeElapsed < 1)
        {
            timeElapsed += Time.deltaTime * 0.25f;

            if (EcosystemManager.Instance.critters.Count > 3)
            {
                for (int i = 0; i < EcosystemManager.Instance.critters.Count; i++)
                {
                    Transform tempTransform = EcosystemManager.Instance.critters[i];
                    EcosystemManager.Instance.RemoveCritter(tempTransform);
                    tempTransform.GetComponent<Blackboard>().SendEvent("Died");
                    Instantiate(projectilePrefab, weaponTop.position, Quaternion.identity).GetComponent<Butllet>().SetProjectile(tempTransform);
                    break;
                }
            }
            yield return 0;
        }

        StartCoroutine(Hide());
    }

    private IEnumerator Hide()
    {
        float timeElapsed = 0;

        while (timeElapsed < 1)
        {
            timeElapsed += Time.deltaTime;

            weaponAnchor.position = Vector2.Lerp(showAnchor.position, hideAnchor.position, weaponShowAnimationCurve.Evaluate(timeElapsed));

            yield return 0;
        }

        SendEvent("Closing");
    }
}
