﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class PodStateLaunching : StateBehaviour
{

    [SerializeField]
    private AnimationCurve landingCurve;

    [SerializeField]
    private ParticleSystem engineParticleSystem;

    [SerializeField]
    private ParticleSystem smokeParticleSystem;

    void Start()
    {

    }

    void OnEnable()
    {
        StartCoroutine(Launch());
    }

    void OnDisable()
    {

    }

    void Update()
    {

    }

    private IEnumerator Launch()
    {
        engineParticleSystem.Play();
        smokeParticleSystem.Play();
        yield return new WaitForSeconds(1);
        float timeElapsed = 0;

        Vector2 startingPosition = transform.position;

        while (timeElapsed < 1)
        {
            timeElapsed += Time.deltaTime * 0.25f;

            transform.position = Vector2.Lerp(startingPosition, new Vector2(startingPosition.x, 20), landingCurve.Evaluate(timeElapsed));

            yield return 0;
        }

        engineParticleSystem.Stop();
        smokeParticleSystem.Stop();

        yield return new WaitForSeconds(20);

        SendEvent("Landing");
    }

}