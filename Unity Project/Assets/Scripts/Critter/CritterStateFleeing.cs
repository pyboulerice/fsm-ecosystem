﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class CritterStateFleeing : StateBehaviour
{

    void Awake()
    {
        //StartCoroutine(Jump());
        direction = blackboard.GetIntVar("Direction");
    }

    void OnEnable()
    {
        StartCoroutine(Jump());
    }

    void OnDisable()
    {

    }

    void Update()
    {

    }

    IntVar direction;

    IEnumerator Jump()
    {
        float jumpStep = 0;
        float movementSpeed = 2;
        Vector3 startingPosition;

        while (true)
        {

			if (Vector2.Distance(transform.position, EcosystemManager.Instance.playerTransform.position) > 4)
			{
				SendEvent("PlayerOutOfRange");
				break;
			}

			if (transform.position.x <= EcosystemManager.Instance.playerTransform.position.x)
			{
				direction.Value = -1;
				transform.localScale = new Vector3(1, 1, 1);
			}
			else if (transform.position.x >= EcosystemManager.Instance.playerTransform.position.x)
			{
				direction.Value = 1;
				transform.localScale = new Vector3(-1, 1, 1);
			}


            startingPosition = transform.position;

			iTween.PunchScale (gameObject, Vector2.one, 0.35f);
            while (jumpStep < 1)
            {
                jumpStep += Time.deltaTime * movementSpeed;
                transform.position = Vector3.Lerp(new Vector3(transform.position.x, startingPosition.y, 0), new Vector3(transform.position.x, startingPosition.y + 1, 0), EcosystemManager.Instance.jumpUpCurve.Evaluate(jumpStep));
                transform.position = Vector3.Lerp(new Vector3(startingPosition.x, transform.position.y, 0), new Vector3(startingPosition.x + direction.Value, transform.position.y, 0), jumpStep);
                yield return 0;
            }

            jumpStep = 0;
            startingPosition = transform.position;

            while (jumpStep < 1)
            {
                jumpStep += Time.deltaTime * movementSpeed;
                transform.position = Vector3.Lerp(new Vector3(transform.position.x, startingPosition.y, 0), new Vector3(transform.position.x, startingPosition.y - 1, 0), EcosystemManager.Instance.fallDownCurve.Evaluate(jumpStep));
                transform.position = Vector3.Lerp(new Vector3(startingPosition.x, transform.position.y, 0), new Vector3(startingPosition.x + direction.Value, transform.position.y, 0), jumpStep);
                yield return 0;
            }

            jumpStep = 0;

            yield return 0;
        }
    }
}
