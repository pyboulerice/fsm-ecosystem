﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class CritterStateLayingEgg : StateBehaviour
{

    void Awake()
    {

    }

    void OnEnable()
    {
		StartCoroutine (LayEgg());
    }

    void OnDisable()
    {

    }

    void Update()
    {

    }

	private IEnumerator LayEgg()
	{
		iTween.ScaleTo (gameObject, transform.localScale * 2, 0.5f);
		yield return new WaitForSeconds (0.5f);
		Instantiate (EcosystemManager.Instance.eggPrefab, transform.position, Quaternion.identity);
		iTween.ScaleTo (gameObject, transform.localScale * 0.5f, 0.5f);
		yield return new WaitForSeconds (0.5f);
		SendEvent ("Roaming");
	}
}
