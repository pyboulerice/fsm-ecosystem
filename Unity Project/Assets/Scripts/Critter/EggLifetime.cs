﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggLifetime : MonoBehaviour {

	[SerializeField]
	private GameObject critterPrefab;

	void Awake () {
		StartCoroutine (LifeTimer());
	}

	private IEnumerator LifeTimer()
	{
		yield return new WaitForSeconds (2);


		Instantiate (critterPrefab, transform.position, Quaternion.identity);
		Destroy (gameObject);
	}
}
