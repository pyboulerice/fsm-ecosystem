﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class CritterStateDying : StateBehaviour
{
    [SerializeField]
    private GameObject deathParticles;

    void Awake()
    {

    }

    private void Start()
    {

    }

    void OnEnable()
    {
        StartCoroutine(Die());
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    void Update()
    {

    }

    private IEnumerator Die()
    {
        iTween.PunchScale(gameObject, Vector2.one * 4, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Instantiate(deathParticles, transform.position, Quaternion.identity);
        iTween.ScaleTo(gameObject, Vector2.zero, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
}
 