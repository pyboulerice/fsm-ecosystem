﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class CritterStateRoaming : StateBehaviour {

    void Awake()
    {
        //StartCoroutine(Jump());
        direction = blackboard.GetIntVar("Direction");
		fed = blackboard.GetBoolVar("Fed");
    }

    private void Start()
    {
        EcosystemManager.Instance.AddCritter(transform);
    }

    void OnEnable()
    {
        StartCoroutine(Jump());
    }

    void OnDisable()
    {
		StopAllCoroutines ();
    }

    void Update()
    {

    }

    IntVar direction;

	BoolVar fed;

    IEnumerator Jump()
    {
        yield return new WaitForSeconds(0.5f);
        float jumpStep = 0;
        float movementSpeed = 2;
        Vector3 startingPosition;

        while (true)
        {
            startingPosition = transform.position;

			iTween.PunchScale (gameObject, Vector2.one, 0.5f);
            while (jumpStep < 1)
            {
                jumpStep += Time.deltaTime * movementSpeed;
                transform.position = Vector3.Lerp(new Vector3(transform.position.x, startingPosition.y, 0), new Vector3(transform.position.x, startingPosition.y + 1, 0), EcosystemManager.Instance.jumpUpCurve.Evaluate(jumpStep));
                transform.position = Vector3.Lerp(new Vector3(startingPosition.x, transform.position.y, 0), new Vector3(startingPosition.x + direction.Value, transform.position.y, 0), jumpStep);
                yield return 0;
            }

            jumpStep = 0;
            startingPosition = transform.position;

            while (jumpStep < 1)
            {
                jumpStep += Time.deltaTime * movementSpeed;
                transform.position = Vector3.Lerp(new Vector3(transform.position.x, startingPosition.y, 0), new Vector3(transform.position.x, startingPosition.y - 1, 0), EcosystemManager.Instance.fallDownCurve.Evaluate(jumpStep));
                transform.position = Vector3.Lerp(new Vector3(startingPosition.x, transform.position.y, 0), new Vector3(startingPosition.x + direction.Value, transform.position.y, 0), jumpStep);
                yield return 0;
            }

			iTween.PunchScale (gameObject, Vector2.up, 0.45f);

            jumpStep = 0;

			CheckWalls ();

			if (Vector2.Distance(transform.position, EcosystemManager.Instance.playerTransform.position) < 4)
			{
				SendEvent("PlayerInRange");
				break;
			}

			if (fed.Value) {
				LayEgg ();
			} else {
				CheckPlants ();
			}

			yield return new WaitForSeconds(0.5f);
        }
    }

	private void CheckWalls()
	{
		if (transform.position.x >= EcosystemManager.Instance.rightWall.position.x)
		{
			direction.Value = -1;
			transform.localScale = new Vector3(1, 1, 1);
		}
		else if (transform.position.x <= EcosystemManager.Instance.leftWall.position.x)
		{
			direction.Value = 1;
			transform.localScale = new Vector3(-1, 1, 1);
		}
	}

	private void LayEgg()
	{
		int ranNumber = Random.Range (1, 101);

		if (ranNumber > 80) {
			SendEvent ("LayingEgg");
			fed.Value = false;
		}
	}

	private void CheckPlants()
	{
		int ranNumber = Random.Range (1, 101);

		if (ranNumber > 50) {
			for (int i = 0; i < EcosystemManager.Instance.plants.Length; i++)
			{
				if (Vector2.Distance (EcosystemManager.Instance.plants [i].transform.position, transform.position) < 1) {
					if (EcosystemManager.Instance.plants [i].GetComponent<Blackboard> ().GetBoolVar ("Edible")) {
						EcosystemManager.Instance.plants [i].GetComponent<Blackboard> ().SendEvent ("Died");
						fed.Value = true;
						break;
					}
				}
			}
		}
	}
}
