﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class EcosystemManager : MonoBehaviour {

    [Header ("Animation Curve")]
    public AnimationCurve jumpUpCurve;
    public AnimationCurve fallDownCurve;

    public AnimationCurve monsterMovementCurve;

    [Header ("World Anchors")]
    public Transform rightWall;
    public Transform leftWall;

    public Transform groundAnchor;

    [Header ("Player")]
    public Transform playerTransform;

    [Header("Monster Anchors")]
    public Transform[] monsterPositionAnchors;
    public Transform hideAnchor;

    [Header ("Prefabs")]
    public GameObject eggPrefab;

    [Header ("Parent Anchors")]
	public Transform plantParent;

	public GameObject[] plants;

    public static EcosystemManager Instance;

    private void Awake()
    {
        Instance = this;

		plants = GameObject.FindGameObjectsWithTag ("Plant");
    }

    public List<Transform> critters = new List<Transform>();

    public void AddCritter(Transform newCritter)
    {
        critters.Add(newCritter);
    }

    public void RemoveCritter(Transform newCritter)
    {
        critters.Remove(newCritter);
    }
}
