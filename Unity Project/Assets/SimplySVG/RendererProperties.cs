﻿// Copyright © 2015 NordicEdu Ltd.

using UnityEngine;
using System.Collections;

namespace SimplySVG {
    //[RequireComponent(typeof(Renderer))]
    public class RendererProperties : MonoBehaviour {

        Renderer render;

        public int layerId = int.MaxValue;
        public int order = int.MaxValue;

        public void SetRenderLayer(int layerId) {
            GetTargetRenderer().sortingLayerID = layerId;
        }

        public void SetRenderOrder(int order) {
            GetTargetRenderer().sortingOrder = order;
        }

        public void Save()
        {
            layerId = GetTargetRenderer().sortingLayerID;
            order = GetTargetRenderer().sortingOrder;
        }

        void OnEnable()
        {
            if (layerId != int.MaxValue)
            {
                SetRenderLayer(layerId);
            }
            if (order != int.MaxValue)
            {
                SetRenderOrder(order);
            }
        }

        public Renderer GetTargetRenderer() {
            if (render == null)
            {
                render = GetComponent<Renderer>();
            }
            if (Application.isEditor && render == null)
            {
                Debug.LogError("Simply SVG renderer properties should only be within game objects that have a renderer!");
            }

            return render;
        }
    }
}
