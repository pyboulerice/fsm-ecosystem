﻿// Copyright © 2015 NordicEdu Ltd.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.IO;
//using System;

namespace SimplySVG {
    public class SimplySVGImporter {
        public string name = null;
        public ImportSettings importSettings = null;

        private Object svgFile;
        public SVGDocument document = null;
        public string svgData;

        // Results
        public Mesh mesh = null;
        public CollisionShapeData collisionShapeData = null;
        public string errors = "";

        //Warning handler
        //key=error, value=lines of document
        private Dictionary<string, List<int>> unsupportedElements = null;

#if UNITY_EDITOR
        int unsupportedFeatureWarningCount = 0;

        /// <summary>
        /// Constructor when in EDITOR
        /// </summary>
        /// <param name="svgFile"></param>
        /// <param name="settings"></param>
        public SimplySVGImporter(Object svgFile, ImportSettings settings = null)
            : this(GetSVGDataFromFile(svgFile), svgFile.name, settings)
        {
            this.svgFile = svgFile;        
        }
#endif
        /// <summary>
        /// Constructor when in RUNTIME. Remember to call Import() and Build() methods after construction.
        /// </summary>
        /// <param name="svgData">text/xml content of a svg file</param>
        /// <param name="name">Name of the object</param>
        /// <param name="settings">Quality settings used while importing</param>
        public SimplySVGImporter(string svgData, string name, ImportSettings settings = null)
        {
            this.svgData = svgData;
            this.name = name;

            importSettings = settings;
            if (importSettings == null)
            {
                importSettings = ScriptableObject.CreateInstance<ImportSettings>();
            }
        }

#if UNITY_EDITOR
        private static string GetSVGDataFromFile(Object svgFile)
        {
            return EditorUtilities.ReadTextFile(svgFile);
        }
#endif

        public void Import() {

            if (string.IsNullOrEmpty(this.svgData))
            {
                Debug.LogError("SVG data was null or empty");
                return;
            }

            DocumentParser docBuilder = new DocumentParser();

            XmlReaderSettings readerSettings = new XmlReaderSettings();

#if UNITY_EDITOR || !(UNITY_WSA || UNITY_WSA_8_0 || UNITY_WSA_8_1 || UNITY_WSA_10_0)
            readerSettings.ProhibitDtd = false;
            readerSettings.XmlResolver = null;
#else
            readerSettings.DtdProcessing = DtdProcessing.Ignore;
#endif

            using (XmlReader reader = XmlReader.Create(new StringReader(this.svgData), readerSettings)) {
                while (reader.Read()) {
                    switch (reader.NodeType) {
                        case XmlNodeType.Element:
                            if (reader.Name == "svg") {
                                document = docBuilder.BeginDocument();

                            } else {
                                string type = reader.Name;

                                // Check if the element is self terminating
                                bool elementIsEmpty = reader.IsEmptyElement;

                                bool elementRecognized = docBuilder.BeginElement(type);
                                if (!elementRecognized) {
                                    WarnAboutUnsupportedFeature(type + " element", reader);

                                    if (elementIsEmpty) {
                                        docBuilder.EndElement();
                                    }

                                    continue;

                                } else {
                                    while (reader.MoveToNextAttribute()) {
                                        string attributeName = reader.Name;

                                        if (!docBuilder.AddAttribute(attributeName, reader.Value)) {
                                            WarnAboutUnsupportedFeature(attributeName + " attribute on " + type + " element", reader);
                                        }
                                    }
                                }

                                if (elementIsEmpty) {
                                    docBuilder.EndElement();
                                }
                            }
                            break;

                        case XmlNodeType.Text:
                            break;

                        case XmlNodeType.XmlDeclaration:
                            break;

                        case XmlNodeType.ProcessingInstruction:
                            break;

                        case XmlNodeType.Comment:
                            break;

                        case XmlNodeType.EndElement:
                            if (reader.Name == "svg") {
                                docBuilder.EndDocument();

                            } else {
                                docBuilder.EndElement();
                            }

                            break;
                    }
                }
            }
            //Check if unsupported features needs a show
            ShowUnsupportedFeatureIfNeeded();
        }

        private void ShowUnsupportedFeatureIfNeeded()
        {
            if (unsupportedElements != null)
            {
                StringBuilder sb = new StringBuilder("Unsupported SVG features detected while importing the SVG document at:\n" + this.name + "\n\nRead the documentation and check your graphics production software's SVG exporter settings. Unless fixed, the graphic may not appear in Unity as intended.\n\nUnsupported features are:\n\n");
                int warnings = 0;
                int maxWarnings = GlobalSettings.Get().maxUnsupportedFeatureWarningCount;
                if(maxWarnings == 0)
                {
                    unsupportedElements = null;

                    return;
                }
                foreach (KeyValuePair<string, List<int>> pair in unsupportedElements)
                {

                    if (++warnings > maxWarnings)
                    {
                        sb.Append("Showing only first " + maxWarnings + " warnings...\n");
                        break;
                    }

                    sb.Append(pair.Key + " at lines:");
                    for (int i = 0; i < pair.Value.Count; ++i)
                    {
                        sb.Append((i==0 ? " " : ", ") + pair.Value[i]);
                    }
                    sb.Append("\n\n");
                }
#if UNITY_EDITOR
                if (!EditorUtility.DisplayDialog("Use of unsupported SVG feature detected!",sb.ToString(),"Ok", "Disable warnings globally"))
                {
                    GlobalSettings.Get().maxUnsupportedFeatureWarningCount = 0;
                    AssetDatabase.SaveAssets();
                }
#else
                errors = sb.ToString();
#endif
                unsupportedElements = null;
            }

        }

        public void WarnAboutUnsupportedFeature(string featureDescription, XmlReader reader = null) {
            if (GlobalSettings.Get().levelOfLog < LogLevel.ERRORS_AND_WARNINGS) {
                return;
            }

            //unsupportedFeatureWarningCount++;
            //if (unsupportedFeatureWarningCount > GlobalSettings.Get().maxUnsupportedFeatureWarningCount) {
            //    return;   
            //}
            
            if (reader != null)
            {
                //Add warning to the dictionary
                if (unsupportedElements == null)
                {
                    unsupportedElements = new Dictionary<string, List<int>>();
                }
                if (!unsupportedElements.ContainsKey(featureDescription))
                {
                    unsupportedElements.Add(featureDescription, new List<int>());
                }
                unsupportedElements[featureDescription].Add(((IXmlLineInfo)reader).LineNumber);
            }
            else
            {

#if UNITY_EDITOR
                if (!EditorUtility.DisplayDialog(
                    "Use of unsupported SVG feature detected!",
                    "Unsupported SVG features detected while importing the SVG document at:\n" + name + "\n\nRead the documentation and check your graphics production software's SVG exporter settings. Unless fixed, the graphic may not appear in Unity as intended.\n\nThe unsupported feature was:\n" + featureDescription + (reader != null ? " (at line " + ((IXmlLineInfo)reader).LineNumber + ")" : "") + (unsupportedFeatureWarningCount == GlobalSettings.Get().maxUnsupportedFeatureWarningCount ? "\n\nWarning limit reached. No more warnings will be given for this SVG document." : ""),
                    "Ok",
                    "Disable warnings globally")
                )
                {
                    GlobalSettings.Get().maxUnsupportedFeatureWarningCount = 0;
                    AssetDatabase.SaveAssets();
                }
#endif
            }
        }

        public bool Build() {
            if (document == null) {
                Debug.LogError("Document has not been imported");
                return false;
            }

            if (!BuildMeshFromDocument(out mesh, out collisionShapeData)) {
                Debug.LogError("Failed to build a mesh from the document at " + name);
                return false;
            }

            return true;
        }

        public bool BuildMeshFromDocument(out Mesh mesh, out CollisionShapeData collisionData) {
            mesh = null;
            collisionData = null;

            List<Vector3> vertices = new List<Vector3>();
            List<int> triangles = new List<int>();
            List<Color> vertexColors = new List<Color>();

            // Start building
            bool documentTriangluatedSuccess = document.Triangulate(
                importSettings,
                ref vertices,
                ref triangles,
                ref vertexColors
            );

            if (!documentTriangluatedSuccess) {
                Debug.LogError("Triangulating the document failed");
                return false;
            }

            if (vertices.Count > 65534) {
                Debug.LogError("Triangulation produced a mesh with more than 65534 vertices. This is a limit imposed by Unity. Cannot continue.");
                return false;

            } else if (vertices.Count < 3) {
                Debug.LogError("Less than 3 vertices were produced when triangulating the document. A mesh cannot be created.");
                return false;
            }

            // Calculate bounds
            Vector3 max = vertices[0];
            Vector3 min = vertices[0];
            for (int i = 0; i < vertices.Count; i++) {
                Vector3 vertex = vertices[i];

                // Check and update bounds values
                min.x = Mathf.Min(min.x, vertex.x);
                min.y = Mathf.Min(min.y, vertex.y);
                min.z = Mathf.Min(min.z, vertex.z);

                max.x = Mathf.Max(max.x, vertex.x);
                max.y = Mathf.Max(max.y, vertex.y);
                max.z = Mathf.Max(max.z, vertex.z);
            }

            // Pivot shift
            Vector3 dim = new Vector3(Mathf.Abs(max.x - min.x), Mathf.Abs(max.y - min.y), 0f);
            Vector3 pivotShift = new Vector3(
                min.x + importSettings.pivot.x * dim.x,
                max.y - importSettings.pivot.y * dim.y, // Use SVG coordinate system
                0f
            );

            Vector3 shiftedMin = min - pivotShift;
            Vector3 shiftedMax = max - pivotShift;
            
            List<Vector2> uv = new List<Vector2>(vertices.Count);

            for (int i = 0; i < vertices.Count; i++) {
                // UV
                uv.Add(new Vector2(
                    (vertices[i].x - min.x) / dim.x,
                    (vertices[i].y - min.y) / dim.y
                ));

                // Shift vertices by pivot offset
                vertices[i] -= pivotShift;

                // Apply uniform import scale
                vertices[i] *= importSettings.scale;
            }

            // Work around a bug in Unity's ParticleSystem
            Color32[] colors32 = new Color32[vertexColors.Count];
            for (int i = 0; i < colors32.Length; i++) {
                colors32[i] = vertexColors[i];
            }

            // Build regular mesh
            mesh = new Mesh();
#if !(UNITY_4 || UNITY_4_6 || UNITY_4_7 || UNITY_5_0 || UNITY_5_1)
            mesh.SetVertices(vertices);
            mesh.SetTriangles(triangles, 0);
            mesh.colors32 = colors32; // Disable after Unity fixes their ParticleSystem
            mesh.SetUVs(0, uv);
#else
            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();
            mesh.colors32 = colors32; // Disable after Unity fixes their ParticleSystem
            mesh.uv = uv.ToArray();
#endif

            Bounds bounds = new Bounds(
                (shiftedMin + (shiftedMax - shiftedMin) / 2f) * importSettings.scale,
                dim * importSettings.scale
            );
            mesh.bounds = bounds;

            ;

            // Build collision shape
            List<Vector2> convexHull = ConvexHullUtility.QuickHull(vertices);
            collisionData = ScriptableObject.CreateInstance<CollisionShapeData>();
            collisionData.Add(convexHull);

            return true;
        }

#if UNITY_EDITOR
        public bool SaveAssets() {
            string svgPath = AssetDatabase.GetAssetPath(svgFile);
            string outputPathBase = svgPath.Substring(0, svgPath.Length - (svgPath.Length - svgPath.LastIndexOf('/'))) + "/";

            string mainAssetPath = outputPathBase + name + ".asset";

            // Check if there is already a asset container for this SVG file
#if !(UNITY_4 || UNITY_4_6 || UNITY_4_7 || UNITY_5_0 || UNITY_5_1)
            Object oldImportSettings = AssetDatabase.LoadAssetAtPath<ImportSettings>(mainAssetPath);
#else
            Object oldImportSettings = AssetDatabase.LoadAssetAtPath(mainAssetPath, typeof(ImportSettings));
#endif
            if (oldImportSettings == null) {
                AssetDatabase.CreateAsset(this.importSettings, mainAssetPath);
                AssetDatabase.SaveAssets();
            }

            if (mesh != null) {
                mesh.name = name + "-mesh";

                // Preserve the old container an only replace changed assets
#if !(UNITY_4 || UNITY_4_6 || UNITY_4_7 || UNITY_5_0 || UNITY_5_1)
                Mesh oldMeshAsset = AssetDatabase.LoadAssetAtPath<Mesh>(mainAssetPath);
#else
                Mesh oldMeshAsset = AssetDatabase.LoadAssetAtPath(mainAssetPath, typeof(Mesh)) as Mesh;
#endif
                if (oldMeshAsset == null) {
                    AssetDatabase.AddObjectToAsset(mesh, mainAssetPath);

                } else {
                    // Replace mesh data
                    oldMeshAsset.Clear();

                    oldMeshAsset.name = mesh.name;
                    oldMeshAsset.vertices = mesh.vertices;
                    oldMeshAsset.colors = null; // Remove old colors from meshes if present. Otherwise they just confuse the buggy ParticleSystem
                    oldMeshAsset.colors32 = mesh.colors32;
                    oldMeshAsset.triangles = mesh.triangles;
                    oldMeshAsset.normals = mesh.normals;
                    oldMeshAsset.uv = mesh.uv;

                    oldMeshAsset.RecalculateBounds();

                    EditorUtility.SetDirty(oldMeshAsset);
                    AssetDatabase.SaveAssets();

                    Object.DestroyImmediate(mesh);
                    mesh = null;
                }
            } else if (GlobalSettings.Get().levelOfLog == LogLevel.ERRORS_AND_WARNINGS) {
                Debug.LogWarning("Mesh missing. Cannot save mesh asset.");
            }

            if (collisionShapeData != null) {
                collisionShapeData.name = name + "-collision";

                // Save collision shape

#if !(UNITY_4 || UNITY_4_6 || UNITY_4_7 || UNITY_5_0 || UNITY_5_1)
                CollisionShapeData oldCollisionShapeData = AssetDatabase.LoadAssetAtPath<CollisionShapeData>(mainAssetPath);
#else
                CollisionShapeData oldCollisionShapeData = AssetDatabase.LoadAssetAtPath(mainAssetPath, typeof(CollisionShapeData)) as CollisionShapeData;
#endif

                if (oldCollisionShapeData == null) {
                    AssetDatabase.AddObjectToAsset(collisionShapeData, mainAssetPath);

                } else {
                    oldCollisionShapeData.Clear();

                    oldCollisionShapeData.name = collisionShapeData.name;
                    oldCollisionShapeData.collisionPolygons = collisionShapeData.collisionPolygons;

                    EditorUtility.SetDirty(oldCollisionShapeData);
                    AssetDatabase.SaveAssets();

                    Object.DestroyImmediate(collisionShapeData);
                    collisionShapeData = null;
                }
            } else if (GlobalSettings.Get().levelOfLog == LogLevel.ERRORS_AND_WARNINGS) {
                Debug.LogWarning("Collision data missing. Cannot save collision asset.");
            }

            // Make sure import settings carries the right name
            // This should have no effect if the ImportSettings object is not a subasset
#if !(UNITY_4 || UNITY_4_6 || UNITY_4_7 || UNITY_5_0 || UNITY_5_1)
            ImportSettings settings = AssetDatabase.LoadAssetAtPath<ImportSettings>(mainAssetPath);
#else
            ImportSettings settings = AssetDatabase.LoadAssetAtPath(mainAssetPath, typeof(ImportSettings)) as ImportSettings;
#endif
            settings.name = name + "-settings";
            AssetDatabase.SaveAssets();

            AssetDatabase.ImportAsset(mainAssetPath);
            return true;
        }
#endif
    }
}
